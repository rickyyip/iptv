import sys
import re


def main():
    argv = sys.argv

    if len(argv) != 3:
        print(f'Invalid number of arguments. Expects 2.')
        return 1

    source_file = argv[1]
    target_file = argv[2]

    print(f'{source_file} => {target_file}')

    s = open(source_file, 'r')
    t = open(target_file, 'w')

    re_comment = re.compile(r'^#')
    re_extinf = re.compile(r'^#EXTINF:')
    re_user_agent = re.compile(r'\ user-agent="[^"]+"')

    count_programs = 0
    count_cleaning = 0

    while True:
        line = s.readline()

        if not line:
            break

        # Count source
        if not re_comment.match(line):
            count_programs += 1
            t.write(line)
            continue

        # Check if the line is EXTINF
        if not re_extinf.match(line):
            t.write(line)
            continue

        # Find and remove user-agent
        user_agent = re_user_agent.search(line)

        if not user_agent:
            t.write(line)
            continue

        clean_line = line.replace(user_agent.group(0), '')
        count_cleaning += 1

        t.write(clean_line)

    s.close()
    t.close()

    print(
        f'Processed {count_programs} programs. Cleaned {count_cleaning}.')


if __name__ == '__main__':
    main()
